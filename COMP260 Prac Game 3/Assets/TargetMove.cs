﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

	private Animator animator;
	public float startTime = 0.0f;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
			animator.SetBool ("Start", Time.time>=startTime);
	}

	public void Destroy (){
		Destroy (gameObject);
	}

	void OnCollisionEnter(Collision collision){
		animator.SetBool("Hit", true);
	}
}
