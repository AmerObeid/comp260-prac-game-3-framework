﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;
	private bool paused = true;

	// Use this for initialization
	void Start () {
		SetPaused(paused);
	}
	
	// Update is called once per frame
	void Update () {
		if (!paused && Input.GetKeyDown (KeyCode.Escape))
			SetPaused (true);
	}

	private void SetPaused(bool p){
		paused = p;
		shellPanel.SetActive (paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay(){
		SetPaused(false);
	}

	public void OnPressedQuit(){
		Application.Quit ();
	}

}
